<?php

/**
 * Use this type of comment block for each class/function you document.
 */
class CommandLine
{
    private static $instance; //singleton pattern contains a static instatce of itself 
    private $functions;

    public static function start()
    {
        if (static::$instance == null) {
            static::$instance = new CommandLine(); /* the construction of a new object is set in the factory class,*/
        }
        }

        return static::$instance;
    }

    protected function __construct() {
        $this->functions = array();
    }

    public function register($command, $code) {
        $this->functions[$command] = $code; 
    }

    public function run($command) {
        if (array_key_exists($command, $this->functions) {
            $this->functions[$command]();
        } else {
            print("That command does not exist\n");
        }
    }
}

// Use in-line comment blocks for specific lines of code.
$cmd = CommandLine::start(); //Transferring the control of the object and their dependencies from the main program to a container/framework
$cmd->register('help', function() {
    print("There is only one function defined and that is 'stop' to exit\n");
});
$cmd->register('stop', function() {
    print("Goodbye\n");
});

$cmd->run('help');
$cmd->run('start');
$cmd->run('stop');