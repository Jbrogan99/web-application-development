<?php

class MapService
{
    private static $instances; // singleton class has only one instance, while providing a global access point to this instance.

    protected function __construct()
    {
    }

    public static function getInstance()
    {
        $calledClass = get_called_class();
        if (!isset(static::$instances[$calledClass]))
        {
            $instances[$calledClass] = new $calledClass(); 
        }
        return static::$instances[$calledClass]; 
    }

    public function lookup($coords)
    {
    }
}

class GoogleMapsService extends MapService
{
}

class OpenStreetMapsService extends MapService
{
}

class AddressLookup {

    public function __construct(MapService $service) //factory pattern used as its instantiate the MapService class 
    {
        $this->$service = $service;
    }

    public function lookupPlace($coords)
    {
        return $this->service.lookup($coords);
    }
}

$lookup = new AddressLookup(OpenStreetMapsService::getInstance());
$lookup->lookupPlace([1.321, 53.122]);