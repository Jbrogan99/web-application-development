<?php

/**
 * Use this type of comment block for each class/function you document.
 */
class Car
{
}

class CarFactory /*CarFactory class provides a static method to get its static instance to outside world.*/
{
    private static $instance; //singleton pattern contains a static instatce of itself 

    protected function __construct()
    {
    }

    public static function new() {
        if (static::$instance == null) { 
            static::$instance = new CarFactory(); /* the construction of a new object is set in the factory class,*/
        }

        return static::$instance;
    }

    public function make()
    {
        return new Car();
    }
}

// Use in-line comment blocks for specific lines of code.
$factory = CarFactory::new(); //creates new object from the CarFactory class 
$car = $factory->make();

$factory2  = CarFactory::new();
$car2 = $factory2->make();