<?php

/**
 * Use this type of comment block for each class/function you document.
 */
class Car
{
}
class Model_Z1 extends Car /*the construction of the car class is being used by each model of the car */
{
}
class Model_Z2 extends Car
{
}

class CarFactory
{
    public function make($type)
    {
        if ($type == 'Z1') {
            return new Model_Z1(); // returns the sub class of car (model_z1) rather than instantiating the object directly .
        } else if ($type == 'Z2') {
            return new Model_Z2();  // returns the sub class of car (model_z2) rather than instantiating the object directly .
        }
    }
}

// Use in-line comment blocks for specific lines of code.
$factory = new CarFactory();
$car = $factory->make('Z2');
$car = $factory->make('Z1');